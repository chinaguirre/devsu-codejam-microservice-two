package com.devsu.codejam.microservicetwo;

import com.devsu.codejam.microservicetwo.representation.ElementsData;
import com.devsu.codejam.microservicetwo.representation.StatisticsElementsRequest;
import com.devsu.codejam.microservicetwo.representation.StatisticsElementsResponse;
import com.google.common.math.Quantiles;
import com.google.common.math.Stats;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class MicroServiceTwoService {

  public StatisticsElementsResponse generateStatisticsWithoutDiscardedElements(StatisticsElementsRequest request) {
    return generateBasicResponse(request);
  }

  public StatisticsElementsResponse generateStatisticsWithDiscardedElements(StatisticsElementsRequest request) {
    List<String> otherElements =
        request.getElements().stream().filter(e -> !(e instanceof Number)).map(String::valueOf).collect(Collectors.toList());
    StatisticsElementsResponse response = generateBasicResponse(request);
    ElementsData addedDiscardedElementsData = response.getData();
    addedDiscardedElementsData.setDiscardedElements(otherElements);
    response.setData(addedDiscardedElementsData);
    return response;
  }

  private StatisticsElementsResponse generateBasicResponse(StatisticsElementsRequest request) {
    List<Number> onlyNumbers =
        request.getElements().stream().filter(e -> e instanceof Number).map(String::valueOf).map(BigDecimal::new).collect(Collectors.toList());
    StatisticsElementsResponse response = new StatisticsElementsResponse();
    response.setData(generateBasicStatsData(onlyNumbers));
    response.setStatus("success");
    response.setMessage("ok");
    return response;
  }

  private ElementsData generateBasicStatsData(List<Number> numbers) {
    Stats stats = Stats.of(numbers);
    ElementsData data = new ElementsData();
    data.setAverage(stats.mean());
    data.setMin(stats.min());
    data.setMax(stats.max());
    data.setCount(stats.count());
    data.setStdev(stats.sampleStandardDeviation());
    data.setMedian(Quantiles.median().compute(numbers));
    Number mode
        = numbers.stream()
        .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
        .entrySet()
        .stream()
        .max(Comparator.comparing(Entry::getValue))
        .get()
        .getKey();
    data.setMode(mode.doubleValue());
    return data;
  }
}
