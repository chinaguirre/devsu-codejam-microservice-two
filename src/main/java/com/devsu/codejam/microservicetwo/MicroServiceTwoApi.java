package com.devsu.codejam.microservicetwo;

import com.devsu.codejam.microservicetwo.representation.StatisticsElementsRequest;
import com.devsu.codejam.microservicetwo.representation.StatisticsElementsResponse;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public interface MicroServiceTwoApi {

//  @PostMapping(value = "/statistics"
//      , produces = {"application/json"},
//      consumes = {"application/json"}
//  )
//  @ResponseStatus(HttpStatus.OK)
//  StatisticsElementsResponse generateGenericStatistics(Authentication authentication, @Valid @RequestBody StatisticsElementsRequest request);

  @PostMapping(value = "/statistics1_0"
      , produces = {"application/json"},
      consumes = {"application/json"}
  )
  @ResponseStatus(HttpStatus.OK)
  StatisticsElementsResponse generateStatisticsWithoutDiscardedElements(@Valid @RequestBody StatisticsElementsRequest request);

  @PostMapping(value = "/statistics1_1"
      , produces = {"application/json"},
      consumes = {"application/json"}
  )
  @ResponseStatus(HttpStatus.OK)
  StatisticsElementsResponse generateStatisticsWithDiscardedElements(@Valid @RequestBody StatisticsElementsRequest request);
}
