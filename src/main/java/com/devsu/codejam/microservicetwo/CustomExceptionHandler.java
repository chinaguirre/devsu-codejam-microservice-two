package com.devsu.codejam.microservicetwo;

import com.devsu.codejam.microservicetwo.representation.StatisticsElementsResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

  private static final String ERROR_VALUE = "error";

  @NonNull
  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> customGeneralExceptionHandleException(@NonNull Exception e, @NonNull WebRequest req) {

    StatisticsElementsResponse response = new StatisticsElementsResponse();
    response.setStatus(ERROR_VALUE);
    response.setMessage(e.getMessage());

    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }


  @Override
  @NonNull
  public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, @NonNull HttpHeaders headers,
      @NonNull HttpStatus status, @NonNull WebRequest request) {
    StatisticsElementsResponse response = new StatisticsElementsResponse();
    response.setStatus(ERROR_VALUE);
    response.setMessage(ex.getMessage());
    return ResponseEntity
        .status(HttpStatus.BAD_REQUEST)
        .body(response);
  }

  @Override
  @NonNull
  public ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, @NonNull HttpHeaders headers,
      @NonNull HttpStatus status, @NonNull WebRequest req) {
    StatisticsElementsResponse response = new StatisticsElementsResponse();
    response.setStatus(ERROR_VALUE);
    response.setMessage(ex.getMessage());
    return ResponseEntity
        .status(HttpStatus.BAD_REQUEST)
        .body(response);
  }
}
