package com.devsu.codejam.microservicetwo.representation;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ElementsData {

  private double mode;

  private double average;

  private double min;

  private double max;

  private double median;

  private double stdev;

  private long count;

  private List<String> discardedElements;

  public double getMode() {
    return mode;
  }

  public void setMode(double mode) {
    this.mode = mode;
  }

  public double getAverage() {
    return average;
  }

  public void setAverage(double average) {
    this.average = average;
  }

  public double getMin() {
    return min;
  }

  public void setMin(double min) {
    this.min = min;
  }

  public double getMax() {
    return max;
  }

  public void setMax(double max) {
    this.max = max;
  }

  public double getMedian() {
    return median;
  }

  public void setMedian(double median) {
    this.median = median;
  }

  public double getStdev() {
    return stdev;
  }

  public void setStdev(double stdev) {
    this.stdev = stdev;
  }

  public long getCount() {
    return count;
  }

  public void setCount(long count) {
    this.count = count;
  }

  public List<String> getDiscardedElements() {
    return discardedElements;
  }

  public void setDiscardedElements(List<String> discardedElements) {
    this.discardedElements = discardedElements;
  }
}
