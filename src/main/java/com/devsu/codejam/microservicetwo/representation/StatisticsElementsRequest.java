package com.devsu.codejam.microservicetwo.representation;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

@Validated
public class StatisticsElementsRequest {

  @JsonProperty("elements")
  @NotNull(message = "elements is mandatory")
  private List<Object> elements;

  public List<Object> getElements() {
    return elements;
  }

  public void setElements(List<Object> elements) {
    this.elements = elements;
  }
}
