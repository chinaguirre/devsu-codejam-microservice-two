package com.devsu.codejam.microservicetwo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

@Configuration
public class MicroServiceTwoSecurity extends WebSecurityConfigurerAdapter {

  private static final String DEFAULT_ROLE = "ADMIN";

  private final boolean authentication;

  @Autowired
  public MicroServiceTwoSecurity(@Qualifier("authentication") boolean authentication) {
    this.authentication = authentication;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    if (authentication) {
      http.csrf().disable().authorizeRequests()
          .antMatchers(HttpMethod.GET, "/**").permitAll()
          .antMatchers(HttpMethod.POST, "/**").authenticated()
          .and()
          .exceptionHandling()
          .accessDeniedHandler(accessDeniedHandler())
          .authenticationEntryPoint(authenticationEntryPoint())
          .and()
          .httpBasic()
          .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    } else {
      http.csrf().disable().authorizeRequests()
          .antMatchers(HttpMethod.GET, "/**").permitAll()
          .antMatchers(HttpMethod.POST, "/**").permitAll()
          .and().httpBasic()
          .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
  }

  @Bean
  public AuthenticationFailureHandler customAuthenticationFailureHandler() {
    return new CustomAuthenticationFailureHandler();
  }

  @Bean
  RestAccessDeniedHandler accessDeniedHandler() {
    return new RestAccessDeniedHandler();
  }

  @Bean
  RestAuthenticationEntryPoint authenticationEntryPoint() {
    return new RestAuthenticationEntryPoint();
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth)
      throws Exception {
    auth.inMemoryAuthentication()
        .withUser("ibm_test_robot_1")
        .password("{noop}ibm_test_1.123#")
        .roles(DEFAULT_ROLE)
        .and()
        .withUser("ibm_test_robot_2")
        .password("{noop}ibm_test_2.123#")
        .roles(DEFAULT_ROLE)
        .and()
        .withUser("ibm_test_robot_3")
        .password("{noop}ibm_test_3.123#")
        .roles(DEFAULT_ROLE);
  }
}
