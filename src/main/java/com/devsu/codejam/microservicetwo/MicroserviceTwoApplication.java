package com.devsu.codejam.microservicetwo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class MicroserviceTwoApplication {

  @Value("${spring.microservice-two.authentication}")
  private boolean authentication;

  public static void main(String[] args) {
    SpringApplication.run(MicroserviceTwoApplication.class, args);
  }

  @Bean
  public RestTemplate getRestTemplate() {
    return new RestTemplate();
  }

  @Bean
  @Qualifier("authentication")
  boolean provideDefaultAuthentication() {
    return authentication;
  }

}
