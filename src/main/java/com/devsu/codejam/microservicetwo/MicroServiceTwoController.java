package com.devsu.codejam.microservicetwo;

import com.devsu.codejam.microservicetwo.representation.StatisticsElementsRequest;
import com.devsu.codejam.microservicetwo.representation.StatisticsElementsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class MicroServiceTwoController implements MicroServiceTwoApi {

  private final MicroServiceTwoService service;
  private final RestTemplate restTemplate;

  @Autowired
  public MicroServiceTwoController(MicroServiceTwoService service, RestTemplate restTemplate) {
    this.service = service;
    this.restTemplate = restTemplate;
  }

//  @Override
//  public StatisticsElementsResponse generateGenericStatistics(Authentication authentication, @Valid StatisticsElementsRequest request) {
//    String userName = authentication.getName();
//    HttpHeaders headers = new HttpHeaders();
//    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
//    headers.setContentType(MediaType.APPLICATION_JSON);
//    HttpEntity<StatisticsElementsRequest> entity = new HttpEntity<>(request, headers);
//    if (userName.equalsIgnoreCase("ibm_test_robot_1")) {
//      return restTemplate.exchange("http://localhost:8060/ibmchallengemic2/statistics1_0", HttpMethod.POST, entity, StatisticsElementsResponse.class)
//          .getBody();
//    }
//    return restTemplate.exchange("http://localhost:8070/ibmchallengemic2/statistics1_1", HttpMethod.POST, entity, StatisticsElementsResponse.class)
//        .getBody();
//  }

  @Override
  public StatisticsElementsResponse generateStatisticsWithoutDiscardedElements(StatisticsElementsRequest request) {
    return service.generateStatisticsWithoutDiscardedElements(request);
  }

  @Override
  public StatisticsElementsResponse generateStatisticsWithDiscardedElements(StatisticsElementsRequest request) {
    return service.generateStatisticsWithDiscardedElements(request);
  }
}
